const { rewriteProjection } = require('../../src/reformulation-engine/projection-operator');

describe('rewriteProjection', () => {
  it('handles inclusions correctly', () => {
    const query = {
      name: 1,
      price: '1',
      'pet.name': true,
    };
    const dictionary = {
      name: ['name', 'firstname'],
      price: ['price'],
      'pet.name': ['petName', 'pet.name'],
    };
    const expectedQuery = {
      name: 1,
      firstname: 1,
      price: '1',
      'pet.name': true,
      petName: true,
    };
    expect(rewriteProjection(query, dictionary)).toStrictEqual(expectedQuery);
  });

  it('handles exclusions correctly', () => {
    const query = {
      name: 0,
      price: '0',
      'pet.name': false,
    };
    const dictionary = {
      name: ['name', 'firstname'],
      price: ['price'],
      'pet.name': ['petName', 'pet.name'],
    };
    const expectedQuery = {
      name: 0,
      firstname: 0,
      price: '0',
      'pet.name': false,
      petName: false,
    };
    expect(rewriteProjection(query, dictionary)).toStrictEqual(expectedQuery);
  });

  it('handles $ operators correctly', () => {
    const query = {
      'authors.$': 1,
      'student.reviews.$': 1,
    };
    const dictionary = {
      authors: ['book.authors', 'authors'],
      'student.reviews': ['reviews', 'student.reviews'],
    };
    const expectedQuery = {
      'book.authors.$': 1,
      'authors.$': 1,
      'reviews.$': 1,
      'student.reviews.$': 1,
    };
    expect(rewriteProjection(query, dictionary)).toStrictEqual(expectedQuery);
  });

  it('handles $slice operators correctly', () => {
    const query = {
      authors: {
        $slice: 5,
      },
    };
    const dictionary = {
      authors: ['authors', 'user.authors'],
    };
    const expectedQuery = {
      authors: {
        $slice: 5,
      },
      'user.authors': {
        $slice: 5,
      },
    };
    expect(rewriteProjection(query, dictionary)).toStrictEqual(expectedQuery);
  });

  it('handles $meta operators correctly', () => {
    const query = {
      authors: {
        $meta: 'textScore',
      },
    };
    const dictionary = {
      authors: ['authors', 'user.authors'],
    };
    const expectedQuery = {
      authors: {
        $meta: 'textScore',
      },
      'user.authors': {
        $meta: 'textScore',
      },
    };
    expect(rewriteProjection(query, dictionary)).toStrictEqual(expectedQuery);
  });

  it('handles $elemMatch operators correctly', () => {
    const query = {
      authors: {
        $elemMatch: {
          school: 102,
        },
      },
    };
    const dictionary = {
      authors: ['authors', 'user.authors'],
    };
    const expectedQuery = {
      authors: {
        $elemMatch: {
          school: 102,
        },
      },
      'user.authors': {
        $elemMatch: {
          school: 102,
        },
      },
    };
    expect(rewriteProjection(query, dictionary)).toStrictEqual(expectedQuery);
  });
});
