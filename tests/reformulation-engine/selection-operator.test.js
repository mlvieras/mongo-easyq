const { convertToExplicitAnd, rewriteSelect } = require('../../src/reformulation-engine/selection-operator');

describe('Selection operator', () => {
  describe('convertToExplicitAnd', () => {
    it('should handle empty queries correctly', () => {
      const andQuery = convertToExplicitAnd({});

      expect(andQuery).toStrictEqual({});
    });

    it('should handle queries with only one equality key correctly', () => {
      const andQuery = convertToExplicitAnd({
        price: 1,
      });

      expect(andQuery).toStrictEqual({
        price: 1,
      });
    });

    it('should handle queries with only one logical key correctly', () => {
      const andQuery = convertToExplicitAnd({
        $or: [
          {
            price: 1,
          },
        ],
      });

      expect(andQuery).toStrictEqual({
        $or: [
          {
            price: 1,
          },
        ],
      });
    });

    it('should handle queries with only one and query correctly', () => {
      const andQuery = convertToExplicitAnd({
        $and: [
          {
            price: 1,
          },
          {
            name: 1,
          },
        ],
      });

      expect(andQuery).toStrictEqual({
        $and: [
          {
            price: 1,
          },
          {
            name: 1,
          },
        ],
      });
    });

    it('should handle a query correctly', () => {
      const query = {
        price: 1,
        $or: [
          {
            $and: [
              { name: 'Peter' },
              { lastName: 'Jones' },
            ],
          },
          {
            $not: {
              id: 15,
            },
          },
        ],
      };
      const expectedQuery = {
        $and: [
          { price: 1 },
          {
            $or: [
              {
                $and: [
                  { name: 'Peter' },
                  { lastName: 'Jones' },
                ],
              },
              {
                $not: {
                  id: 15,
                },
              },
            ],
          },
        ],
      };

      expect(convertToExplicitAnd(query)).toStrictEqual(expectedQuery);
    });

    it('should handle a query with a root $and', () => {
      const query = {
        $and: [
          { movie: 'Star Wars' },
          { author: 'Tolkien' },
        ],
        price: 1,
        $or: [
          {
            $and: [
              { name: 'Peter' },
              { lastName: 'Jones' },
            ],
          },
          {
            $not: {
              id: 15,
            },
          },
        ],
      };
      const expectedQuery = {
        $and: [
          { movie: 'Star Wars' },
          { author: 'Tolkien' },
          { price: 1 },
          {
            $or: [
              {
                $and: [
                  { name: 'Peter' },
                  { lastName: 'Jones' },
                ],
              },
              {
                $not: {
                  id: 15,
                },
              },
            ],
          },
        ],
      };

      expect(convertToExplicitAnd(query)).toStrictEqual(expectedQuery);
    });

    it('should not modify a query with only a root $and', () => {
      const query = {
        $and: [
          { price: 1 },
          { name: 'Peter' },
        ],
      };
      const expectedQuery = {
        $and: [
          { price: 1 },
          { name: 'Peter' },
        ],
      };

      expect(convertToExplicitAnd(query)).toStrictEqual(expectedQuery);
    });
  });

  describe('rewriteSelect', () => {
    it('should convert an empty query correctly', () => {
      const query = {};
      const dictionary = {};
      expect(rewriteSelect(query, dictionary)).toStrictEqual({});
    });

    it('should convert leaf nodes correctly', () => {
      const query = {
        price: 1,
        name: {
          $gt: 'name',
          $lt: 'name2',
        },
        age: {
          $or: [
            {
              $gt: 15,
            },
            {
              $lt: 30,
            },
          ],
        },
      };
      const dictionary = {
        price: ['price', 'product.price'],
        name: ['name', 'firstname'],
        age: ['age'],
      };
      const expectedQuery = {
        $and: [
          {
            $or: [
              {
                price: 1,
              },
              {
                'product.price': 1,
              },
            ],
          },
          {
            $or: [
              {
                name: {
                  $gt: 'name',
                  $lt: 'name2',
                },
              },
              {
                firstname: {
                  $gt: 'name',
                  $lt: 'name2',
                },
              },
            ],
          },
          {
            $or: [
              {
                age: {
                  $or: [
                    {
                      $gt: 15,
                    },
                    {
                      $lt: 30,
                    },
                  ],
                },
              },
            ],
          },
        ],
      };
      expect(rewriteSelect(query, dictionary)).toStrictEqual(expectedQuery);
    });
  });
});
