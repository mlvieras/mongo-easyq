const { connect } = require('./setup');
const { DATABASE_NAME, COLLECTION_NAME } = require('./constants');
const { reformulateFind } = require('../src/reformulation-engine');
const { updateSchema, generateDictionary } = require('../src/dictionary-generator');

const AMOUNT_OF_REWRITES = 100000;

const rewriteTimingExperiment = async () => {
  const client = await connect();
  try {
    const db = client.db(DATABASE_NAME);
    const collection = db.collection(COLLECTION_NAME);
    const cursor = collection.find({});
    const docCount = await collection.countDocuments();
    console.log(`Collection has ${docCount.toLocaleString()} documents.`);

    console.log('Starting schema generation...');
    let schema = [];
    await cursor.batchSize(500).forEach((doc) => {
      schema = updateSchema(schema, [doc]);
    });
    console.log(`  - Schema has ${schema.length} paths.`);
    console.log('Starting dictionary generation...');
    const dictionary = generateDictionary(schema);
    console.log(`  - Dictionary has ${Object.keys(dictionary).count} keys.`);

    console.log(`Starting benchmark, executing rewrite ${AMOUNT_OF_REWRITES} times.`);

    const selectionQuery = {
      $or: [
        {
          country: 'United States',
        },
        {
          country: 'Argentina',
        },
        {
          country: 'Chile',
        },
        {
          year: 2000,
          'director.first_name': 'Peter',
        },
      ],
      'genres.0': 'Action',
      last_name: 'O\'Neal',
    };

    const projectionQuery = {
      _id: false,
      year: true,
      country: true,
      genres: {
        $slice: 2,
      },
    };

    const startDate = new Date();
    for (let i = 0; i < AMOUNT_OF_REWRITES; i += 1) {
      reformulateFind(dictionary, selectionQuery, projectionQuery);
    }
    const endDate = new Date();
    const endTime = endDate - startDate;

    console.log();
    console.log('Time results:');
    console.log(`Total Time:      ${endTime.toLocaleString()}ms`);
    console.log();
  } catch (error) {
    throw error;
  } finally {
    client.close();
  }
};

module.exports = {
  rewriteTimingExperiment,
};
