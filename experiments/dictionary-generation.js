const path = require('path');
const fs = require('fs-extra');

const { connect } = require('./setup');
const { DATABASE_NAME, COLLECTION_NAME, RESULTS_DIR } = require('./constants');
const { updateSchema, generateDictionary } = require('../src/dictionary-generator');

const SCHEMA_FILENAME = 'schema.json';
const DICTIONARY_FILENAME = 'dictionary.json';

const dictionaryGenerationExperiment = async () => {
  const startDate = new Date();
  const client = await connect();
  try {
    const db = client.db(DATABASE_NAME);
    const collection = db.collection(COLLECTION_NAME);
    const cursor = collection.find({});
    const docCount = await collection.countDocuments();
    console.log(`Collection has ${docCount.toLocaleString()} documents.`);

    console.log('Starting schema generation...');
    const schemaStartDate = new Date();
    let schema = [];
    await cursor.batchSize(500).forEach((doc) => {
      schema = updateSchema(schema, [doc]);
    });
    const schemaTime = new Date() - schemaStartDate;
    console.log(`  - Schema has ${schema.length} paths.`);
    console.log('Starting dictionary generation...');
    const dictionaryStartDate = new Date();
    const dictionary = generateDictionary(schema);
    const dictionaryTime = new Date() - dictionaryStartDate;
    console.log(`  - Dictionary has ${Object.keys(dictionary).count} keys.`);

    console.log('Saving results...');
    fs.ensureDirSync(RESULTS_DIR);
    fs.writeFileSync(path.join(RESULTS_DIR, SCHEMA_FILENAME), JSON.stringify(schema));
    fs.writeFileSync(path.join(RESULTS_DIR, DICTIONARY_FILENAME), JSON.stringify(dictionary));
    const endTime = new Date() - startDate;

    console.log();
    console.log('Time results:');
    console.log(`Total Time:      ${endTime.toLocaleString()}ms`);
    console.log(`Schema Time:     ${schemaTime.toLocaleString()}ms`);
    console.log(`Dictionary Time: ${dictionaryTime.toLocaleString()}ms`);
    console.log();
  } catch (error) {
    throw error;
  } finally {
    client.close();
  }
};

module.exports = {
  dictionaryGenerationExperiment,
};
