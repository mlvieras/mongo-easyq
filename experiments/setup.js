const { MongoClient } = require('mongodb');

const { MONGO_URL } = require('./constants');

const connect = () => new MongoClient(MONGO_URL, { useNewUrlParser: true }).connect();

module.exports = {
  connect,
};
