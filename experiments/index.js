const { dictionaryGenerationExperiment } = require('./dictionary-generation');
const { rewriteTimingExperiment } = require('./rewrite-timing');

const VALID_EXPERIMENTS = {
  DICTIONARY: 'dictionary',
  REWRITE_TIMING: 'rewriteTiming',
};

const getExperiment = () => {
  const validExperiments = Object.values(VALID_EXPERIMENTS).join(', ');
  if (process.argv.length < 3) {
    throw new Error(`Experiment not passed. Valid experiments are [${validExperiments}]`);
  } else if (!Object.values(VALID_EXPERIMENTS).includes(process.argv[2])) {
    throw new Error(`Experiment invalid, received ${process.argv[2]}. Valid experiments are [${validExperiments}]`);
  }
  return process.argv[2];
};

const main = async () => {
  const experiment = getExperiment();
  if (experiment === VALID_EXPERIMENTS.DICTIONARY) {
    await dictionaryGenerationExperiment();
  } else if (experiment === VALID_EXPERIMENTS.REWRITE_TIMING) {
    await rewriteTimingExperiment();
  }
};

main()
  .then(() => { console.log('Experiment ended correctly.'); })
  .catch((error) => { throw error; });
