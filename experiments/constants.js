const path = require('path');

const MONGO_URL = 'mongodb://127.0.0.1:27017';
const DATABASE_NAME = 'bdnr-loaded';
const COLLECTION_NAME = 'movies';
const RESULTS_DIR = path.join(__dirname, 'results');

module.exports = {
  MONGO_URL,
  DATABASE_NAME,
  COLLECTION_NAME,
  RESULTS_DIR,
};
