// Operators extracted from: https://docs.mongodb.com/manual/reference/operator/query/
// NOTE: only some are provided
const QUERY_OPERATORS = {
  LOGICAL: {
    AND: '$and',
    NOT: '$not',
    NOR: '$nor',
    OR: '$or',
  },
};

/**
 * Returns true if `key` is a Mongo query key
 * @param {string} key key of a Mongo query.
 *
 * @return {boolean}
 */
const isQueryKey = key => key && key.startsWith('$');

/**
 * Given a Mongo query, returns a query semantically identical
 * which has explicit AND conditions everywhere.
 *
 * @param {Object} query Mongo query to convert.
 *
 * @return {Object} transformed query.
 */
const convertToExplicitAnd = (query) => {
  const andQueries = [];
  Object.entries(query).forEach(([key, value]) => {
    if (isQueryKey(key)) {
      if (key === QUERY_OPERATORS.LOGICAL.NOT) {
        andQueries.push({
          [key]: convertToExplicitAnd(value),
        });
      } else if (key === QUERY_OPERATORS.LOGICAL.AND) {
        value.forEach((entry) => {
          andQueries.push(convertToExplicitAnd(entry));
        });
      } else {
        andQueries.push({
          [key]: value.map(convertToExplicitAnd),
        });
      }
    } else {
      andQueries.push({
        [key]: value,
      });
    }
  });
  if (!andQueries.length) {
    return {};
  }
  if (andQueries.length === 1) {
    return andQueries[0];
  }
  return {
    $and: andQueries,
  };
};

/**
 * Recursive. Given a Mongo query or a subtree of one and a dictionary, rewrites the query
 * to expand its semantics according to the dictionary.
 *
 * @param {Object} node a Mongo query or a subtree of one.
 * @param {Object} dictionary
 *
 * @return {Object} transformed query.
 */
const rewriteNode = (node, dictionary) => {
  const newQuery = {};
  Object.entries(node).forEach(([key, value]) => {
    if (isQueryKey(key)) {
      if (key === QUERY_OPERATORS.LOGICAL.NOT) {
        newQuery[key] = rewriteNode(value, dictionary);
      } else {
        newQuery[key] = value.map(subQuery => rewriteNode(subQuery, dictionary));
      }
    } else {
      const newKeys = dictionary[key];
      if (newKeys) {
        const orQuery = newKeys.map(newKey => ({
          [newKey]: value,
        }));
        newQuery.$or = newQuery.$or || [];
        orQuery.forEach((entry) => {
          newQuery.$or.push(entry);
        });
      }
    }
  });
  return newQuery;
};

/**
 * Given a Mongo query and a dictionary, rewrites the query
 * to expand its semantics according to the dictionary.
 *
 * @param {Object} node a Mongo query.
 * @param {Object} dictionary
 *
 * @return {Object} transformed query.
 */
const rewriteSelect = (originalQuery, dictionary) => {
  // If the original query is not an object, it is returned as is because
  // this function acts like a middleware.
  if (!originalQuery || typeof originalQuery !== 'object' || Array.isArray(originalQuery)) {
    return originalQuery;
  }
  return rewriteNode(convertToExplicitAnd(originalQuery), dictionary);
};

module.exports = {
  convertToExplicitAnd,
  rewriteSelect,
};
