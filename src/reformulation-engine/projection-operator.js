const INCLUSION_OPERATORS = [
  '1',
  1,
  true,
];
const EXCLUSION_OPERATORS = [
  '0',
  0,
  false,
];

const is$Operator = key => key && key.endsWith('.$');

const rewriteProjection = (originalQuery, dictionary) => {
  // If the original query is not an object, it is returned as is because
  // this function acts like a middleware.
  if (!originalQuery || typeof originalQuery !== 'object' || Array.isArray(originalQuery)) {
    return originalQuery;
  }

  const newQuery = {};

  Object.entries(originalQuery).forEach(([key, value]) => {
    if (!is$Operator(key)
      && (INCLUSION_OPERATORS.includes(value) || EXCLUSION_OPERATORS.includes(value))
    ) {
      dictionary[key].forEach((newKey) => {
        newQuery[newKey] = value;
      });
    } else if (is$Operator(key)) {
      const pathToArray = key.substring(0, key.length - 2);
      dictionary[pathToArray].forEach((newKey) => {
        newQuery[`${newKey}.$`] = value;
      });
    } else if (value && typeof value === 'object' && !Array.isArray(value)) {
      const newObjectValue = {};
      if (value.$elemMatch) {
        newObjectValue.$elemMatch = value.$elemMatch;
      }
      if (value.$slice) {
        newObjectValue.$slice = value.$slice;
      }
      if (value.$meta) {
        newObjectValue.$meta = value.$meta;
      }
      dictionary[key].forEach((newKey) => {
        newQuery[newKey] = newObjectValue;
      });
    }
  });
  return newQuery;
};

module.exports = {
  rewriteProjection,
};
