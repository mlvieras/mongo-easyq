const { rewriteSelect } = require('./selection-operator');
const { rewriteProjection } = require('./projection-operator');

const reformulateFind = (dictionary, selectionQuery, projectionQuery) => ([
  rewriteSelect(selectionQuery, dictionary),
  rewriteProjection(projectionQuery, dictionary),
]);

module.exports = {
  reformulateFind,
};
