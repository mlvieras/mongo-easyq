
const VANILLA_PROTOTYPES = [
  Object.prototype,
  Array.prototype,
];

/**
 * Given any kind of value (primitive or object), returns true only if
 * `target` is instance of any class except for vanilla Object and Array.
 *
 * @param {*} target any Javascript primitive or object on which to do the check.
 *
 * @return {boolean} wether or not `target` is an instance of a non-vanilla class.
 */
const isInstanceOfClass = (target) => typeof target === 'object' && !VANILLA_PROTOTYPES.includes(Object.getPrototypeOf(target));

module.exports = {
  isInstanceOfClass,
};
