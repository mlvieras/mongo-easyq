const { generateDictionary } = require('./generator');
const { updateSchema, schemaFromDocuments } = require('./schema-generator');

module.exports = {
  generateDictionary,
  schemaFromDocuments,
  updateSchema,
};
