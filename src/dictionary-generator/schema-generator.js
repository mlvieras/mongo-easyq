const { isInstanceOfClass } = require('../utils/validator');

/**
 * Recursive. Given a node and a base path, returns all absolute paths from {@link node}
 * prefixed by {@link basePath}.
 *
 * @param {string} basePath - used as a base to any paths determined on this node.
 * Useful to make {@link nodeSchema} a recursive function.
 * @param {(Object|Array|null|undefined|string)} node - structure which schema must be calculated.
 *
 * @return {Object} - Schema in object form.
 */
const nodeSchema = (basePath, node) => {
  // Always add the base path as a possible path.
  const schema = {
    [basePath]: null,
  };
  // NOTE: if an object that isn't instance of Object or Array is encountered,
  // the script WON'T attempt to access and index the instance. This is specially
  // useful for the `_id` field, which is instance of ObjectId, and has internal properties
  // which should never be queried.
  if (typeof node === 'object' && node !== null && !isInstanceOfClass(node)) {
    if (Array.isArray(node)) {
      node.forEach((nodeItem, index) => (
        Object.assign(schema, nodeSchema(`${basePath}.${index}`, nodeItem))
      ));
      return schema;
    }
    Object.entries(node).forEach(([key, value]) => (
      Object.assign(schema, nodeSchema(`${basePath}.${key}`, value))
    ));
  }
  return schema;
};

/**
 * Given a document, returns its schema in object form.
 *
 * @param {Object} document - Javascript object with arbitrary keys and depth.
 *
 * @return {Object} Schema of the document in object form.
 */
const documentSchema = (document) => {
  const schema = {};

  Object.entries(document).forEach(([key, value]) => {
    Object.assign(schema, nodeSchema(key, value));
  });

  return schema;
};

/**
 * Given an array of documents, calculates the collection schema.
 * The collection schema contains all absolute paths that exist in the collection.
 *
 * @param {Object[]} documents An array of documents. Documents can have any shape and any depth
 * as long as they are Javascript Objects.
 *
 * @return {string[]} The resulting schema.
 */
const schemaFromDocuments = (documents) => {
  const schema = {};

  documents.forEach((document) => {
    Object.assign(schema, documentSchema(document));
  });

  return Object.keys(schema);
};

/**
 * Given a schema and an array of documents, returns a new
 * schema that contains the same paths as {@link currentSchema} plus
 * all new paths discovered on {@link newDocuments}.
 *
 * @param {string[]} currentSchema Schema to be updated
 * @param {Object[]} newDocuments New document collection to update the schema with.
 *
 * @return {string[]} Updated schema
 */
const updateSchema = (currentSchema, newDocuments) => {
  const newSchema = {};
  currentSchema.forEach((entry) => { newSchema[entry] = null; });

  const arraySchema = schemaFromDocuments(newDocuments);
  arraySchema.forEach((entry) => { newSchema[entry] = null; });
  return Object.keys(newSchema);
};

module.exports = {
  schemaFromDocuments,
  updateSchema,
};
