/**
 *
 * Given a collection schema, generates all possible relative document
 * paths. Note that only valid paths will be returned, this means that
 * paths starting with an array index are ignored.
 *
 * @param {string[]} schema Schema to use as base.
 *
 * @return {string[]}
 */
const documentPathsFromSchema = (schema) => {
  const documentPaths = {};

  schema.forEach((absolutePath) => {
    let currentSuffix = '';
    absolutePath.split('.').reverse().forEach((entry) => {
      if (!currentSuffix) {
        currentSuffix = entry;
      } else {
        currentSuffix = `${entry}.${currentSuffix}`;
      }
      // You can't have a path that starts with a number, so
      // they have to be filtered out.
      if (/\d+/.test(entry)) {
        return;
      }
      documentPaths[currentSuffix] = null;
    });
  });

  return Object.keys(documentPaths);
};

/**
 *
 * @param {string[]} schema Schema from which to generate the dictionary.
 *
 * @return {Object}
 */
const generateDictionary = (schema) => {
  const dictionary = {};
  const documentPaths = documentPathsFromSchema(schema);
  documentPaths.forEach((relativePath) => {
    const matchingSchemaPaths = [];
    schema.forEach((absolutePath) => {
      if (absolutePath.endsWith(relativePath)) {
        matchingSchemaPaths.push(absolutePath);
      }
    });
    dictionary[relativePath] = matchingSchemaPaths;
  });
  return dictionary;
};

module.exports = {
  generateDictionary,
};
