# Consultas en colecciones MongoDB heterogéneas y sin esquema

En este repositorio se encuentra la entrega del trabajo realizado para la materia Bases de Datos no relacionales, dictada por la Facultad de Ingeniería de la Universidad de la República del Uruguay. El código presente en este proyecto se basa en el trabajo de Hamadou et al: [*Schema-independent Querying for Heterogeneous Collections in NoSQL Document Stores*](https://www.researchgate.net/publication/332292112_Schema-independent_Querying_for_Heterogeneous_Collections_in_NoSQL_Document_Stores).

## Resumen

En este documento se encuentran detalladas las tecnologías necesarias para ejecutar el código entregado, así como los pasos para realizar un setup. El código se encuentra debidamente cubierto por tests unitarios, que también pueden ser ejecutados.

El proyecto se encuentra distribuído en directorios, cuyo cometido se detalla a continuación:

- **experiments:** conjunto de scripts que permiten la realización de experimentos automáticos, que emiten resultados al terminar.
- **src/dictionary-generator:** código relativo a la generación de diccionario y esquemas.
- **src/reformulation-engine:** código relativo a la reformulación de consultas Mongo.
- **src/utils:** utilidades genéricas.
- **tests:** conjunto de tests unitarios.

## Setup

### Dependencias

- MongoDB v4.0.10
- Node 12.6.0
- NPM 6.9.0

### Instalación

Para lograr ejecutar el proyecto es necesario instalar la versión 12.6.0 de Node. Es recomendable utilizar un manejador automático de versiones de Node, como lo es [nodenv](https://github.com/nodenv/nodenv) (el utilizado para este proyecto), o bien [nvm](https://github.com/nvm-sh/nvm). Luego, instalar la versión apropiada de [MongoDB](https://www.mongodb.com/), según el sistema operativo utilizado.

Luego, es necesario instalar los paquetes de los que depende el proyecto:

```bash
npm install -g npm@6.9.0 # Instala la versión de NPM a utilizar
npm install
```

### Tests

Para iniciar y correr los tests se debe ejecutar el siguiente comando:

```bash
npm test
```

## Ejecución de experimentos

En el proyecto se presentan dos experimentos:

- Creación de un diccionario.
- Tiempo de reescritura de consultas.

Se provee un comando a nivel de NPM que permite ejecutar los experimentos:

```bash
npm run experiments EXPERIMENTO
```

Donde *EXPERIMENTO* es uno de los dos experimentos posibles. Ejecutar el comando sin explicitar este valor mostrará la lista de valores posibles.

### Detalles a considerar

Para lograr ejecutar los experimentos, el servidor de Mongo debe estar corriendo en la máquina. En Ubuntu esto se consulta de la siguiente forma:

```bash
sudo service mongo status
```

Luego, es necesario descargar alguno de los [datasets](https://www.irit.fr/recherches/SIG/SDD/EASY-QUERY/index.html) publicados por los investigadores. Luego, se debe importar en una colección de Mongo utilizando el comando `mongoimport`:

```bash
mongoimport -d NOMBRE_BASE -c NOMBRE_COLECCION ARCHIVO_JSON
```

Donde *NOMBRE_BASE* es el nombre de la base de datos a utilizar (o crear), *NOMBRE_COLECCION* es el nombre de la colección a la que agregar los documentos, y *ARCHIVO_JSON* es el archivo JSON a ingresar. Para algunos datasets se exportan varios archivos JSON, de los cuales se puede seleccionar uno para importar.

Puede ser necesario editar el archivo de constantes, ubicado en [./experiments/constants.js](./experiments/constants.js), modificando las constantes definidas para apuntar a la colección y base de datos deseada. La herramienta hará uso de esa colección para los experimentos.
